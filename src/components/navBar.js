import { Link, NavLink } from "react-router-dom";


import './styles.css'
function NavBar() {
    return ( <nav className="navBar">
<NavLink to="/">Home</NavLink>
<NavLink to="/about">About</NavLink>
<NavLink to="/books">Books</NavLink>


    </nav> );
}

export default NavBar;