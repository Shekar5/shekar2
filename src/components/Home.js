import React from "react";
import { useNavigate } from "react-router-dom";

function Home() {
  const navigate = useNavigate();
  return (
    <div style={{ margin: "20px",button:{padding:'5px'} }}>
      click here to go to{" "}
      <button style={{ margin: "20px",padding:'5px' }} onClick={() => navigate("books")}>Books</button>
      {/* <button style={{ margin: "20px",padding:'5px' }} onClick={()=>navigate(1)}>go forward</button> */}
    </div>
  );
}

export default Home;
