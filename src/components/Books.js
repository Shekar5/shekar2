import {React,useState} from "react";
import { Link, Outlet } from "react-router-dom";
import { useNavigate,useSearchParams } from "react-router-dom";
const books = [
  { id: 1, title: "book1", content: "viewing book1" },
  { id: 2, title: "book2", content: "viewing book2" },
  { id: 3, title: "book3", content: "viewing book3" },

];


function Books() {
    const navigate=useNavigate()
    const [booksToDisplay,setBooks]=useState(books);
    const [searchTerm, setSearchTerm]=useState();
    const [searchParam,setSearchParam]=useSearchParams({});
 
    const handleSearch=(e)=>{
        searchParam=setSearchParam({filter:e.target.value})
        if(e.target.value==''){
            setSearchParam({});
        }
      
    }
  return (
    <><div>
        <input type='search' value={searchTerm} placeholder='search books' onChange={(e)=>handleSearch(e)}/>
    </div>
      {" "}
      <div style={{ display: "flex", flexDirection: "row", margin: "10px" }}>
        {booksToDisplay.map((book) => {
          return<><div style={{ margin: "10px" }}> <Link to={book.title}>{book.title}</Link></div></>;
        })}
        
      </div>
      <button onClick={()=>navigate(-1)}>go back</button>
      <hr style={{ borderTop: "1px solid black" }} />
      <Outlet/>
      <div style={{margin: '400px',display:'flex', flexDirection:'row'}}> click here to know the book price
      {booksToDisplay.map((book) => {
          return<><div style={{ margin: "10px" }}> <Link to={`/books/${book.id}`}>{book.title}</Link></div></>;
        })}</div>
    </>
  );
}

export default Books;
